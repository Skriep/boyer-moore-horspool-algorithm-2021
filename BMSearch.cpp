﻿#include <iostream>
#include <string>

int BMFindTarget(std::string text, std::string target);
int BMFindTargetOptimized(std::string text, std::string target);
void CalculateOffsetTable(std::string target, int BMT[256]);

int main() {
	std::string text, target;
	std::cout << "Enter any string to search in:" << std::endl;
	std::getline(std::cin, text);
	std::cout << "Enter some string to search for:" << std::endl;
	std::getline(std::cin, target);
	while (target.length() == 0) {
		std::cout << "Length of this string cannot be zero! Please try again." << std::endl;
		std::getline(std::cin, target);
	}
	std::cout << "Result of the optimized algorithm:\n" << BMFindTargetOptimized(text, target) << std::endl;
	std::cout << "Result of the original algorithm:\n" << BMFindTarget(text, target) << std::endl;
	std::cout << "Press any key to exit...";
	getchar();
	return 0;
}

int BMFindTargetOptimized(std::string text, std::string target) {
	int text_length = text.length();
	int target_length = target.length();
	int BMT[256];
	CalculateOffsetTable(target, BMT);

	int pos = target_length - 1;
	while (pos < text_length) {
		for (int i = target_length - 1; i >= 0; i--) {
			if (target[i] != text[pos - target_length + i + 1]) {
				pos += BMT[(short)(text[pos])];
				break;
			} else if (i == 0) {
				return pos - target_length + 1;
			}
		}
	}
	return -1;
 }

int BMFindTarget(std::string text, std::string target) {
	 int text_length = text.length();
	 int target_length = target.length();
	 int BMT[256];
	 CalculateOffsetTable(target, BMT);

	 int pos = target_length - 1;
	 while (pos < text_length) {
		 if (target[target_length - 1] != text[pos]) {
			 pos += BMT[(short)(text[pos])];
		 } else {
			 for (int i = target_length - 2; i >= 0; i--) {
				 if (target[i] != text[pos - target_length + i + 1]) {
					 // Infinite cycle is possible,
					 // beсause right side is sometimes 0.
					 pos += BMT[(short)(text[pos - target_length + i + 1])] - 1;
					 break;
				 } else if (i == 0) return pos - target_length + 1;
			 }
		 }
	 }
	 return -1;
 }

void CalculateOffsetTable(std::string target, int BMT[256]) {
	const int target_length = target.length();
	for (int i = 0; i < 256; i++) {
		BMT[i] = target_length;
	}
	for (int i = 0; i < target_length - 1; i++) {
		BMT[(short)(target[i])] = target_length - i - 1;
	}
}